// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  hmr       : false,
  apiUrl    : 'http://localhost:8080',
  // apiUrl    : 'http://staging.fotia.com.my/api-wv',
  // webpage    : 'http://staging.fotia.com.my/weddingvalet-web'
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

function loadJSON(filePath) {
  const json = loadTextFileAjaxSync(filePath, "application/json");
  return JSON.parse(json);
}

function loadTextFileAjaxSync(filePath, mimeType) {
  const xmlhttp = new XMLHttpRequest();
  xmlhttp.open("GET", filePath, false);
  if (mimeType != null) {
      if (xmlhttp.overrideMimeType) {
          xmlhttp.overrideMimeType(mimeType);
      }
  }
  xmlhttp.send();
  if (xmlhttp.status == 200) {
      return xmlhttp.responseText;
  }
  else {
      return null;
  }
}
