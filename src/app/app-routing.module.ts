import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FullComponent } from './layouts/full/full.component';
import { BlankComponent } from './layouts/blank/blank.component';

export const Approutes: Routes = [
  {
    path: '',
    component: FullComponent,
    children: [
      { 
        path: '', 
        redirectTo: '/dashboard/admin', 
        pathMatch: 'full',
      },
      {
        path: 'dashboard',
        loadChildren: './main/pages/dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'profile',
        loadChildren: './main/pages/profile/profile.module#ProfileModule',
      },
      {
        path: 'user',
        loadChildren: './main/pages/user-management/user-management.module#UserManagementModule',
      },
    ]
  },
  {
    path: '',
    component: BlankComponent,
    children: [
      {
        path: 'auth',
        loadChildren:
          './main/pages/auths/authentication.module#AuthenticationModule'
      }
    ]
  },
  {
    path: '**',
    redirectTo: '/auth/login'
  }
];
