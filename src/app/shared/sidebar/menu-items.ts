import { RouteInfo } from './sidebar.metadata';

export const ROUTES: RouteInfo[] = [
  {
    path: '/dashboard/admin',
    title: 'Dashboard',
    icon: 'mdi mdi-view-dashboard',
    class: '',
    extralink: false,
    submenu: []
  },
  {
    path: '',
    title: 'User Management',
    icon: 'mdi mdi-account-settings-variant',
    class: 'has-arrow',
    extralink: false,
    submenu: [
      {
        path: '/user/listing',
        title: 'User Listing',
        icon: 'mdi mdi-account-multiple',
        class: '',
        extralink: false,
        submenu: []
      },
      {
        path: '/user/listing',
        title: 'User Attendance',
        icon: 'mdi mdi-clock-start',
        class: '',
        extralink: false,
        submenu: []
      }
    ]
  },
  {
    path: '',
    title: 'Leave Management',
    icon: 'mdi mdi-calendar-today',
    class: 'has-arrow',
    extralink: false,
    submenu: [
      {
        path: '/user/listing',
        title: 'Leave Application',
        icon: 'mdi mdi-calendar-plus',
        class: '',
        extralink: false,
        submenu: []
      },
      {
        path: '/user/listing',
        title: 'Leave Listing',
        icon: 'mdi mdi-calendar-multiple-check',
        class: '',
        extralink: false,
        submenu: []
      },
      {
        path: '/user/listing',
        title: 'Replacement Application',
        icon: 'mdi mdi-calendar-multiple',
        class: '',
        extralink: false,
        submenu: []
      }
    ]
  },
  {
    path: '',
    title: 'Claims Management',
    icon: 'mdi mdi-cash',
    class: 'has-arrow',
    extralink: false,
    submenu: [
      {
        path: '/user/listing',
        title: 'Claims Listing',
        icon: 'mdi mdi-cash-multiple',
        class: '',
        extralink: false,
        submenu: []
      }
    ]
  },
  {
    path: '/dashboard/admin',
    title: 'Payroll',
    icon: 'mdi mdi-receipt',
    class: '',
    extralink: false,
    submenu: []
  },
];
