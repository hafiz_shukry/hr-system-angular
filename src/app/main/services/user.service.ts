import { Injectable } from "@angular/core";
import { HttpHeaders, HttpClient } from "@angular/common/http";
import { environment } from "../../../environments/environment";
import { Observable } from "rxjs";
import { tap, catchError } from "rxjs/operators";
import { User } from "../model/user/user";
import { Page } from "./helper/page";
import { PagedData } from "./helper/paged-data";
import { ErrorHandlerService } from "./helper/error.handler.service";


const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({ providedIn: 'root' })
export class UserService {

    private userUrl = `http://localhost:8080/user`;

    constructor(
        private http: HttpClient,
        private _error: ErrorHandlerService,

    ) { }

  getUserListing(page: Page): Observable<PagedData<User>>{
      return this.http.post(this.userUrl + '/listing' , page)
      .pipe(
          tap(),
          catchError(this._error.handleErrorStatus<any>())
      )

  }
}
