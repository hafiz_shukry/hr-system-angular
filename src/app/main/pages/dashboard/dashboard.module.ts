import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { FormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { ChartistModule } from 'ng-chartist';
import { PerfectScrollbarModule } from "ngx-perfect-scrollbar";
import { NgxDatatableModule } from "@swimlane/ngx-datatable";
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { MatSlideToggleModule, MatIconModule, MatButtonModule } from "@angular/material";
import { DashboardComponent } from "./dashboard.component";
import { WeatherheaderComponent } from "./dashboard-component/weather-header/weather-header.component";
import { ProjectComponent } from "./dashboard-component/project/project.component";

const routes = [
    {
        path    :'admin',
        component   : DashboardComponent,
        data: {
            title: 'Admin Dashboard',
            urls: [
                { title: 'Dashboard' }
            ]
        }
    },

];

@NgModule({
    declarations:[
        DashboardComponent,
        ProjectComponent,
        WeatherheaderComponent

    ],

    imports:[
        RouterModule.forChild(routes),
        FormsModule,
        CommonModule,
        NgbModule,
        PerfectScrollbarModule,
        NgxChartsModule,
        NgxDatatableModule,
        MatSlideToggleModule,
        MatIconModule,
        MatButtonModule,
        NgxChartsModule,
        ChartistModule,

    ]
})
export class DashboardModule
{
    
}