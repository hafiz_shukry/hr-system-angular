import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/main/services/user.service';
import { ToastrService } from 'ngx-toastr';
import { PasswordValidation } from 'src/app/main/common/password-validation';

@Component({
    selector: 'profile',
    templateUrl: './profile.component.html',
})


export class ProfileComponent implements OnInit {
    
    constructor(
        private formBuilder: FormBuilder,
        private _router: Router,
        private route: ActivatedRoute,
        private _userService: UserService,
        private _toaster: ToastrService,
   

    ) {
            }



    ngOnInit() {
      
       
    }

    onSubmit() {
       
    }

}
