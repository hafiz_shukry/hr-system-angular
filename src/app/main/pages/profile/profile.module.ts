import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { MatIconModule, MatButtonModule, MatFormFieldModule, MatSelectModule, MatRadioButton, MatRadioModule, MatDatepickerModule, MatInputModule, MatNativeDateModule } from "@angular/material";
import { SweetAlert2Module } from "@toverux/ngx-sweetalert2";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { CommonModule } from "@angular/common";
import { ProfileComponent} from "./profile.component"


const routes = [
    {
        path    :':id',
        component : ProfileComponent,
    },
    
];
@NgModule({
    declarations:[
        ProfileComponent
    ],

    imports:[
    RouterModule.forChild(routes),
      FormsModule,
      ReactiveFormsModule,
      NgbModule,
      MatIconModule,
      MatButtonModule,
      MatFormFieldModule,
      SweetAlert2Module,
      CommonModule,
      MatSelectModule,
      MatRadioModule,
      MatDatepickerModule,
      MatInputModule,
      MatNativeDateModule


    ]
})
export class ProfileModule
{
    
}