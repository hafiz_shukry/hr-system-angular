import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { PasswordValidation } from '../../../common/password-validation';
import { User } from '../../../model/user/user';
import { MatDialog } from '@angular/material';

@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    // providers: [AuthService]
})
export class SignupComponent implements OnInit {

    constructor(
        private _router: Router,
        private formBuilder: FormBuilder,
        private _toastr: ToastrService,
        private dialog: MatDialog

    ) {
    }


    ngOnInit() {
    }

    onSubmit() {
      
    }


    login() {
        this._router.navigate(['/auth/login'])
    }
}

