import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { FormsModule, ReactiveFormsModule } from "@angular/forms"
import { MatButtonModule, MatFormFieldModule } from "@angular/material";
import { CommonModule } from '@angular/common';
import { SignupComponent } from "./signup.component";

const routes = [
    {
        path    :'signup',
        component   : SignupComponent,
        data: {
            title: 'Sign Up',
            urls: [
                { title: 'Signup Page' }
            ]
        }
            
        
    },

];
@NgModule({
    declarations:[
        SignupComponent,
        
    ],

    imports:[
        RouterModule.forChild(routes),
        FormsModule,
        ReactiveFormsModule,
        MatButtonModule,
        MatFormFieldModule,
        CommonModule
    ]
})
export class SignupModule
{
    
}