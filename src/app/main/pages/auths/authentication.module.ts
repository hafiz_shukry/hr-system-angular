import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginModule } from './login/login.module';
import { SignupModule } from './signup/signup.module';

@NgModule({
    declarations: [
    ],
    imports: [
        CommonModule,
        LoginModule,
        SignupModule,
    ],
    entryComponents: [
    ]
})
export class AuthenticationModule { }
