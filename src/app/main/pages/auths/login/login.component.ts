import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpParams } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { AuthService as OauthService, GoogleLoginProvider, FacebookLoginProvider } from 'angular5-social-login';
import { User } from '../../../model/user/user';
import { environment } from '../../../../../environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  providers: []
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup

  constructor(
    private _router: Router,
    private _builder: FormBuilder,
    public toaster: ToastrService,
  ) { }

  ngOnInit() {
  }


  login(user: User) {
    
  }
  signup() {
    // this._router.navigate(['/auth/signup'])
  }

}