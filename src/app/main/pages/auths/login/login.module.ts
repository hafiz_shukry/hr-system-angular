import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { FormsModule, ReactiveFormsModule } from "@angular/forms"
import { MatButtonModule, MatFormFieldModule } from "@angular/material";
import { LoginComponent } from "./login.component";
import { CommonModule } from '@angular/common';

const routes = [
    {
        path    :'login',
        component   : LoginComponent,
        data: {
            title: 'Login',
            urls: [
                { title: 'Login Page' }
            ]
        }
            
        
    },

];
@NgModule({
    declarations:[
        LoginComponent,
        
    ],

    imports:[
        RouterModule.forChild(routes),
        FormsModule,
        ReactiveFormsModule,
        MatButtonModule,
        MatFormFieldModule,
        CommonModule
    ]
})
export class LoginModule
{
    
}