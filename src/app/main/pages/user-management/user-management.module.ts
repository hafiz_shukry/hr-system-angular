import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { MatIconModule, MatButtonModule, MatFormFieldModule, MatSelectModule, MatRadioButton, MatRadioModule, MatDatepickerModule, MatInputModule, MatNativeDateModule, MatMenuModule, MatChipsModule } from "@angular/material";
import { SweetAlert2Module } from "@toverux/ngx-sweetalert2";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { CommonModule } from "@angular/common";
import { UserManagementComponent } from "./user-management.component";
import { NgxDatatableModule } from "@swimlane/ngx-datatable";


const routes = [
    {
        path    :'listing',
        component : UserManagementComponent,
    },
    
];
@NgModule({
    declarations:[
        UserManagementComponent
    ],

    imports:[
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    NgxDatatableModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    MatFormFieldModule,
    SweetAlert2Module,
    CommonModule,
    MatSelectModule,
    MatChipsModule


    ]
})
export class UserManagementModule
{
    
}