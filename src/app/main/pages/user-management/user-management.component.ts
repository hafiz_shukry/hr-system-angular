import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Page } from '../../services/helper/page';
import { UserService } from '../../services/user.service';

@Component({
    selector: 'user-management',
    templateUrl: './user-management.component.html',
    styleUrls: ['./user-management.component.scss']

})


export class UserManagementComponent implements OnInit {
    rows: any[];
    temp = [];
    page = new Page();
    loadingIndicator = true;

    constructor(
        private formBuilder: FormBuilder,
        private _router: Router,
        private route: ActivatedRoute,
        private _toaster: ToastrService,
        private _user: UserService

    ) {
        this.page.pageNumber = 0;
        this.page.size = 10;
        this.page.sortField = "firstname";
        this.page.sort = "asc";
    }



    ngOnInit() {
        this.setPage({ offset: 0 });
    }

    setPage(pageInfo){
        this.page.pageNumber = pageInfo.offset;
        this._user.getUserListing(this.page).subscribe(
            pagedData => {
                this.page = pagedData.page;
                this.rows = pagedData.data;
                this.loadingIndicator = false;
            }
        )
    }

    onSort(event) {

        var field = event.sorts[0].prop;
        var sort =  event.sorts[0].dir;

        this.page.sortField = field;
        this.page.sort = sort;

        this._user.getUserListing(this.page).subscribe(
            pagedData => {
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
        });

    }
    updateFilter(event) {
        const val = event.target.value.toLowerCase();
        this.page.search = val;
    
        this._user.getUserListing(this.page).subscribe(
            pagedData => {
                    this.page = pagedData.page;
                    this.rows = pagedData.data;
                    this.loadingIndicator = false;
        });
    }

}
