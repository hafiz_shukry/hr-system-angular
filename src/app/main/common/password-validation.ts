import { AbstractControl } from "@angular/forms";

export class PasswordValidation {

    static pattern: string = '(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}';

    static MatchPassword(AC: AbstractControl) {
       let newPassword = AC.get('newPassword').value; // to get value in input tag
       let confirmPass = AC.get('confirmPass').value; // to get value in input tag
        if(newPassword != confirmPass) {
            AC.get('confirmPass').setErrors( {MatchPassword: true} )
        } else {
            return null
        }
    }
}